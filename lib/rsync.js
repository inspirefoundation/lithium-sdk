'use strict';

var fs = require('fs-extra');
var { exec } = require('child_process');
var Rsync = require('rsync');

module.exports = function (src, dest) {
  return new Promise(function (resolve, reject) {
    let command = new Rsync()
      .source(src)
      .destination(dest)
      .update()
      .set('out-format', '%f')
      .flags('i')
      .recursive()
      .command();

    if (process.platform === 'win32') {
      command = `wsl ${command}`;
    }

    fs.ensureDir(dest, function () {
      exec(command, function (error, stdout) {
        let hasError = false;
        if (error) {
          hasError = true;
          if (error.message.indexOf('rsync exited with code 24') === -1) {
            reject(error.message);
          }
        }
        let filesSynced = [];
        if (!hasError) {
          filesSynced = stdout.split('\n').filter(function (value) {
            return value.length > 0;
          });
        }
        resolve(filesSynced);
      });
    });
  });
};
